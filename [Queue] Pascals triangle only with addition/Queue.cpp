#include "Queue.h"
#include <vector>
#include <iostream>
Queue::Queue(){
    l = 0;
    p = 0;
    _vec.resize(int_max);
}

void Queue::Enqueue(int e){
    try{
    if (l == int_max)
    {
        throw QUEUE_FULL;
    }
    else
    {
        l++;
        _vec[((p-1+l) % int_max)] = e;
    }}
    catch(ERROR){
      std::cout << "The queue is not long enough \n";}
} //Enqueue

int Queue::Dequeue(){
     try{
    if (l == 0)
    {
        throw QUEUE_EMPTY;
    }else{
    int out = _vec[p];
    p = ((p+1) % int_max);
    l--;
    return out;}}
    catch(ERROR){
      std::cout << "The queue is not long enough \n";}
} //Dequeue

int Queue::Front() const{
    try {
    if(l ==0){
             throw QUEUE_EMPTY;}
    else{
        int out = _vec[p];
        return out;
    }}
    catch(ERROR){
     std::cout << "The queue is not long enough \n";
    }
}// Front

int Queue::Rear() const{
     try {
    if(l ==0){
            throw QUEUE_EMPTY;}
    else{
    int out = _vec[p+l-1];
    return out;
    }
   }
    catch(ERROR){
     std::cout << "The queue is not long enough \n";
    }
}// Front

int Queue::length() const{
    return l;
}// length

void Queue::pascal_trianlges_given_line(int k){
while(k >0){
    int length = l;
    for (int i = 1;i <= length+1 ;++i )
    {
        if (i == 1)
        {
            Enqueue(1);
        }else if (i == length+1){
        Enqueue(Dequeue());
        }
        else {
        Enqueue(Dequeue()+Front());
        }
    }
    k--;
}
}

void Queue::out() const {
for (int i = p ;i <= p+l-1;++i )
{
    std::cout << _vec[i]<< " ";
}
}
