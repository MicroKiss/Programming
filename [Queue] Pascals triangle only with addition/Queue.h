#ifndef QUEUE_H
#define QUEUE_H
#include <vector>
#include<iostream>

class Queue
{
    public:
        enum ERROR{QUEUE_EMPTY,QUEUE_FULL};
        Queue();
        void Enqueue(int e);
        int Dequeue();
        int Front() const ;
        int Rear() const ;
        int length() const ;
        void pascal_trianlges_given_line(int k);
        void out() const ;
    private:
        int l;//length
        int p;//starting position
        std::vector<int> _vec;
        int int_max = 32767;
};

#endif // Queue
