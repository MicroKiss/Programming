#ifndef PAIR_H
#define PAIR_H


class Pair
{
public:
    Pair(){};
    Pair(int a,int b){
        _value = a;
        _quantity = b;
    }

    int GetValue() const{
        return _value;
    }

    int GetQuantity() const{
        return _quantity;
    }

    void SetQuantity(int a){
        _quantity = a;
    }

    Pair& operator=(const Pair &s){
        if (this != &s)
          {
         _value = s._value;
        _quantity = s._quantity;
          }

          return *this;
    }

private:
    int _value;
    int _quantity;
};

#endif // PAIR_H
