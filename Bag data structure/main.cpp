//#define NORMAL_MODE
#ifdef NORMAL_MODE


#include <iostream>
#include <fstream>
#include "Bag.h"

using namespace std;

class Menu
{
public:
    Menu() {};
    void run();
private:
    Bag bag;
    void menuWrite();
    void input();
    void output();
    void pullout();
    void quantity();
    void isempty();
    void common();
    void tests();
    void testWrite();
    void test1();
    void test2();
    void test3();
    void testnfo();
};

int main()
{
//        Bag a;
//    a.putIn(1);
//    a.putIn(2);
//    a.putIn(3);
//    a.putIn(3);
//    a.putIn(4);
//    Bag b = a;
//    b.putIn(2);
//    b.putIn(2);
//    b.putIn(2);
//    b.putIn(2);
//    cout << b;
//    cout << "a\n"<< a;
    Menu m;
    m.run();
    return 42;
}

void Menu::run()
{
    int n = 0;
    do
    {
        menuWrite();
        cout << endl << ">>>>" ;
        cin >> n;
        switch(n)
        {
        case 1:
            input();
            break;
        case 2:
            output();
            break;
        case 3:
            pullout();
            break;
        case 4:
            quantity();
            break;
        case 5:
            isempty();
            break;
        case 6:
            common();
            break;
        case 7:
            tests();
            break;
        }
    }
    while(n!=0);
}

void Menu::menuWrite()
{
    cout << endl << endl;
    cout << " 0. -Kilepes" << endl;
    cout << " 1. -Zsak elemeinek megadasa inputrol" << endl;
    cout << " 2. -Zsak elemeinek kiirasa" << endl;
    cout << " 3. -Elem kivetele a zsakbol" << endl;
    cout << " 4. -Adott ertek darabszama" << endl;
    cout << " 5. -Ures-e a zsak? " << endl;
    cout << " 6. -Zsak metszete egy masikkal " << endl;
    cout << " 7. -Elore megirt teszt esetek" << endl;
}

void Menu::input()
{
    cout << "Adja meg a zsak elemeit es irjon '-4242'-ot amikor befejezte!\n";
    int n = 0;
read:
    cout << ">>" ;
    cin >> n;
    if (n != -4242)
    {
        bag.putIn(n);
        goto read;
    }
}

void Menu::output()
{
    cout <<"Jelenlegi elemek\n" << bag;
}

void Menu::pullout()
{
    int n = 0;
    cout << "Jelenlegi elemek: \n";
    cout << bag << "Irja be az elemeket es irjon '-4242'-ot amikor befejezte\n";
read:
    cout << ">>";
    cin >> n;
    if (n != -4242)
    {

        bag.pullOut(n);

        goto read;
    }
}

void Menu::quantity()
{
    int n;
    cout << "melyik elemre kivancsi ?\n>>";
    cin >> n;

    cout << bag.Quantity(n)<< " szer talalhato meg" <<endl;


}

void Menu::isempty()
{
    if (bag.IsEmpty())
    {
        cout<<" A taska ures\n";
    }
    else
    {
        cout<< "A taska NEM ures\n";
    }
}

void Menu::common()
{
    cout << "Irja be a masodik zsak elemeit ( -4242 a lezaroelem)\n";
    Bag second;
    int n = 0;
read:
    cout << ">>" ;
    cin >> n;
    if (n != -4242)
    {
        second.putIn(n);
        goto read;
    }

    cout <<"A metszet elemei: \n" << bag.Common(second);

}

void Menu::tests()
{
    cout <<"Itt az elore megirt tesztesetekkel probalkozhatunk\n";
    int n;
    do
    {
        testWrite();

        cout << endl << ">>>>" ;
        cin >> n;
        switch(n)
        {
        case 1:
            test1();
            break;
        case 2:
            test2();
            break;
        case 3:
            test3();
            break;
        case 4:
            testnfo();
            break;
        }
    }
    while(n!=0);
}

void Menu::testWrite()
{
    cout << endl << endl;
    cout << " 0. -Vissza a fomenube" << endl;
    cout << " 1. -bemenet kimenet es metszet" << endl;
    cout << " 2. -Nem letezo elem kivetele" << endl;
    cout << " 3. -bagy bemenet" << endl;
    cout << " 4. -Info" << endl;
}


void Menu::test1()
{
    Bag Bag_test1;
    Bag Bag_test2;

    ifstream f;
    f.open( "test1.txt" );
    if(f.fail())
    {
        cout << "Hiba a fajl nyitasakor!\n";
    }
    int e;
    while(f >> e)
    {
        Bag_test1.putIn(e);
    }
    f.close();
    f.open("test2.txt");
    if(f.fail())
    {
        cout << "Hiba a fajl nyitasakor!\n";
    }
    while(f >> e)
    {
        Bag_test2.putIn(e);
    }
    f.close();

    cout << "beolvasott adatok:\n" << "Bag 1 \n" << Bag_test1;
    cout <<  "Bag 2 \n" << Bag_test2;
    cout << "Ezek metszete: \n" << Bag_test1.Common(Bag_test2);
}

void Menu::test2()
{
    Bag Bag_test1;

    ifstream f;
    f.open( "test1.txt" );
    if(f.fail())
    {
        cout << "Hiba a fajl nyitasakor!\n";
    }
    int e;
    while(f >> e)
    {
        Bag_test1.putIn(e);
    }
    f.close();

    cout << "Most megprobaljuk kivenni a -4242 elemet.\n " << ".-.-.-.-.-.-.-.\n";

    Bag_test1.pullOut(-4242);


}

void Menu::test3()
{
    Bag Bag_test1;

    ifstream f;
    f.open( "test3.txt" );
    if(f.fail())
    {
        cout << "Hiba a fajl nyitasakor!\n";
    }
    int e;
    while(f >> e)
    {
        Bag_test1.putIn(e);
    }
    f.close();
    cout << "Az elemek: \n" << Bag_test1;
}


void Menu::testnfo()
{
    cout << "Ha a tesztek nem futnak le akkor a program nem talalja a test1/2/3... fajlokat\n";
    cout << "Ha egyedi teszt eseteket szeretnenk akkor atirhatjuk a test1/2/3...fajlokat\n";
}


#else
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Bag.h"

TEST_CASE("constructor,thing w/ empty bag")
{
    SECTION("constructor make empty bag")
    {
        Bag bag;
        CHECK(bag.IsEmpty() == TRUE );
        CHECK(bag.Getitemcount() == 0);
        CHECK(bag.Getmaxitems() == 128);

    }
    SECTION("doing things with empty bag")
    {
        Bag bag;
        CHECK(bag.Quantity(42) == 0);
    }
}

TEST_CASE("items in and out")
{
    SECTION("in")
    {
        Bag bag;
        bag.putIn(2);
        CHECK(bag.IsEmpty() == FALSE);
        CHECK(bag.Getitemcount() == 1);
        bag.putIn(2);
        CHECK(bag.Getitemcount() == 1);
        bag.putIn(2);
        bag.putIn(0);
        CHECK(bag.Getitemcount() == 2);
        bag.putIn(-65);
        CHECK(bag.Quantity(2) == 3);
    }

    SECTION("manymany")
    {
        SECTION("many of same")
        {
            Bag bag;
            for (int i = 0; i < 4780 ; ++i )
            {
                bag.putIn(2);
            }
            CHECK(bag.Quantity(2) == 4780);
            CHECK(bag.Getitemcount() == 1);
            CHECK(bag.Getmaxitems() == 128);
        }
        SECTION("many different")
        {
            Bag bag;
            for (int i = 0; i < 4780 ; ++i )
            {
                bag.putIn(i);
            }
            CHECK(bag.Getitemcount() == 4780);
            CHECK(bag.Getmaxitems() == 8192);

        }

    }

    SECTION("out")
    {
        Bag bag;
        bag.putIn(2);
        bag.putIn(2);
        bag.putIn(2);
        bag.putIn(0);
        bag.putIn(-65);
        CHECK(bag.IsEmpty() == FALSE);
        CHECK(bag.Quantity(2) == 3);
        bag.pullOut(2);
        bag.pullOut(2);
        bag.pullOut(2);
        bag.pullOut(0);
        bag.pullOut(-65);
        CHECK(bag.IsEmpty() == TRUE);
    }
}

TEST_CASE("Common items")
{

    Bag bag1;
    bag1.putIn(1);
    bag1.putIn(2);
    bag1.putIn(2);
    bag1.putIn(2);
    bag1.putIn(3);
    bag1.putIn(3);
    bag1.putIn(3);
    Bag bag2;
    bag2.putIn(1);
    bag2.putIn(2);
    bag2.putIn(2);
    bag2.putIn(3);
    bag2.putIn(3);
    bag2.putIn(3);
    bag2.putIn(3);
    bag2.putIn(3);
    Bag bag3 = bag1.Common(bag2);
    unsigned int a,b,c;
    a = bag3.Quantity(1);
    b = bag3.Quantity(2);
    c = bag3.Quantity(3);
    CHECK(a == 1);
    CHECK(b == 2);
    CHECK(c == 3);
}

TEST_CASE("copy")
{
    Bag bag1;
    bag1.putIn(1);
    bag1.putIn(2);
    bag1.putIn(2);
    bag1.putIn(2);
    bag1.putIn(3);
    bag1.putIn(3);
    bag1.putIn(3);
    Bag bag2 = bag1;
    bag1.putIn(3);
    CHECK(bag1.Quantity(3) == (bag2.Quantity(3)+1));
    bag2.pullOut(1);
    CHECK(bag2.Quantity(1) == 0);
    CHECK(bag1.Quantity(1) == 1);
}

TEST_CASE("= operator")
{
    Bag bag1;
    bag1.putIn(1);
    bag1.putIn(2);
    bag1.putIn(2);
    bag1.putIn(2);
    bag1.putIn(3);
    bag1.putIn(3);
    bag1.putIn(3);
    Bag bag2;
    bag1 = bag1;
    CHECK(bag1.Quantity(1) == 1);
    bag2 = bag1;
    bag1.putIn(3);
    CHECK(bag1.Quantity(3) == (bag2.Quantity(3)+1));
    bag2.pullOut(1);
    CHECK(bag2.Quantity(1) == 0);
    CHECK(bag1.Quantity(1) == 1);
}

#endif
