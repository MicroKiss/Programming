#ifndef BAG_H
#define BAG_H

#include <iostream>
#include "Pair.h"

class Bag
{
private:
    Pair* Items;
    unsigned int itemcount;
    unsigned int maxitems;
public:
    ~Bag();                                                              /** @jo  destruktor */
    Bag();                                                               /** @jo bag létrehozása  */
    Bag(int itemcount);                                                  /** @jo bag létrehozása adott mérettel  */
    Bag(const Bag &copied);                                              /** @jo  másoló konstruktor */
    Bag& operator=(const Bag& a);                                        /** @jo értékadás operátor */
    void write()const;                                                   /** @jo  zsák kiírása */
    friend   std::ostream& operator<<(std::ostream& s, const Bag& a);    /** @jo  kiíras << -el*/
    bool IsEmpty() const;                                                /** @jo  üres-e a halmaz*/
    void putIn(int e);                                                   /** @jo  elem betevése */
    void pullOut(int e);                                                 /** @jo  elem kivétele */
    int Quantity(int n) const;                                           /** @jo  egy elem hányszor van a zsákban */
    Bag Common(const Bag a) const ;                                      /** @jo  két zsák metszete  */

    /**for the tests*/
    int Getitemcount() const {return itemcount;};
    int Getmaxitems() const {return maxitems;};
};

#endif // BAG_H
