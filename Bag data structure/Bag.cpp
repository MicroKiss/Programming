#include "bag.h"
#include <iostream>
#include <iomanip>
#include <Pair.h>


Bag::~Bag()
{
    delete[] Items;
}
Bag::Bag()
{
    maxitems = 128;

    Items = new Pair[maxitems];
    itemcount = 0;
}

Bag::Bag(int e){
maxitems = e;
Items = new Pair[maxitems];
itemcount = 0;
}


Bag::Bag( const Bag  &copied)    //masolo konstrukt
{
    maxitems = copied.maxitems;
    Items = new Pair[maxitems];
    itemcount = copied.itemcount;
    for (unsigned int i = 0; i < copied.itemcount ; ++i )
    {

        Items[i] = copied.Items[i] ;

    }
}


Bag& Bag::operator=(const Bag& other)  //�rt�kad�s oper�tor     *****************************************
{
    if(this != &other )
    {
        delete[] Items;
        maxitems = other.maxitems;
        Items = new Pair[maxitems];
        itemcount = other.itemcount;

            for (unsigned int i = 0; i < other.itemcount ; ++i)
            {
                Items[i] = other.Items[i];
            }
        }

    return *this;
}

void Bag::write() const  // zs�k ki�r�sa
{
    for(unsigned int i =0;i < itemcount;  ++i)
    {
        std::cout << std::setw(5) << Items[i].GetValue() << std::setw(5) << Items[i].GetQuantity() <<" db\n";
    }
}
std::ostream& operator<<(std::ostream& s, const Bag& a)
{
    if (a.itemcount == 0)
    {
        s << "The bag is empty";
    }
    else
    {
        for(unsigned int i = 0;i < a.itemcount; ++i)
        {
            s << std::setw(5) << a.Items[i].GetValue() << std::setw(5) << a.Items[i].GetQuantity() <<" db\n";
        }
    }
    return s;
}

bool Bag::IsEmpty() const    // �res-e a halmaz
{
    return (0 == itemcount );
}

void Bag::putIn(int e)   // elem betev�se
{
       unsigned int i = 0;

        while (i <itemcount && Items[i].GetValue() != e )
        {
          i++;
        }


        if (i < itemcount )
        {
            Items[i].SetQuantity(Items[i].GetQuantity()+1);
        }
        else
        {
            Items[itemcount++] = Pair(e,1);
        }


    if (itemcount == maxitems)
    {

        maxitems = maxitems << 1;
        Pair* newItems = new Pair[maxitems];
        for (unsigned int i = 0;i < itemcount ;++i )
        {
            newItems[i] = Items[i];
        }
        delete[] Items;
        Items = newItems;
    }
}

void Bag::pullOut(int e)   // elem kiv�tele
{
    unsigned int i = 0;

    while (i < itemcount && Items[i].GetValue() != e)
    {
        i++;
    }
    if (i < itemcount)
    {
        Items[i].SetQuantity(Items[i].GetQuantity()-1);

        if (Items[i].GetQuantity() == 0)
        {

            Items[i] = Pair(Items[itemcount-1].GetValue(),Items[itemcount-1].GetQuantity());
            itemcount--;
        }
    }
}

int Bag::Quantity(int e) const   //egy elem h�nyszor van a zs�kban
{

        unsigned int i = 0;

        while(i < itemcount && e != Items[i].GetValue() )
        {
            i++;

        }
        if (i < itemcount )
        {
            return Items[i].GetQuantity();
        }
        else
        {
            return 0;
        }
}

Bag Bag::Common(const Bag a) const   // k�t zs�k metszete
{
    int smalleritemcount = (itemcount > a.itemcount) ? a.itemcount : itemcount;
    Bag out(smalleritemcount);

        if (itemcount > a.itemcount )
        {
            for(unsigned int cit1 = 0/*a*/; cit1 < a.itemcount;  ++cit1) //i
            {
                bool match = false;

                unsigned int Quantity;

                for(unsigned int cit2 = 0; cit2 < itemcount;  ++cit2) //j
                {
                    if (a.Items[cit1].GetValue() == Items[cit2].GetValue())
                    {
                        match = true;

                        Quantity = (Items[cit2].GetQuantity() > a.Items[cit1].GetQuantity() ? a.Items[cit1].GetQuantity() : Items[cit2].GetQuantity());

                        break;
                    }
                }
                if (match)
                {
                    out.Items[out.itemcount++]= Pair(a.Items[cit1].GetValue(),Quantity);
                }
            }
        }
        else
        {
            for(unsigned int cit1 = 0 ; cit1 < itemcount ; ++cit1) //i
            {
                bool match = false;

                unsigned int Quantity;

                for(unsigned int cit2 = 0; cit2 < a.itemcount;  ++cit2) //j
                {
                    if (Items[cit1].GetValue() == a.Items[cit2].GetValue())
                    {
                        match = true;

                        Quantity = (a.Items[cit2].GetQuantity() > Items[cit1].GetQuantity() ? Items[cit1].GetQuantity() : a.Items[cit2].GetQuantity());

                        break;
                    }
                }
                if (match)
                {
                    out.Items[out.itemcount++] = (Pair(Items[cit1].GetValue(),Quantity));
                }
            }

        }

        return out;
}

