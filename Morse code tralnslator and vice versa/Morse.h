#ifndef MORSE_H
#define MORSE_H
#include <vector>
#include <string>
#include <Code.h>
class Morse
{
    public:
    Morse();
    std::string ToMorse(std::string input) const;
    std::string FromMorse(std::string input);
    void newchar(char ch,std::string str);
    void write();
    private:
        std::vector<Code> _vec;
};

#endif // MORSE_H
