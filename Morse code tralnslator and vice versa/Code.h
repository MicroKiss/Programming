#ifndef CODE_H
#define CODE_H


class Code
{
    public:
        Code(char ch,std::string code): ch(ch),code(code){};
    std::string GetCode() const{return code;};

    char GetChar() const{return ch;};

    private:
        char ch;
        std::string code;
};

#endif // CODE_H
