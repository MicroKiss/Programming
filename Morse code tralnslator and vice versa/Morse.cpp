#include "Morse.h"
#include <iostream>

Morse::Morse()
{
    //ctor
}

std::string Morse::ToMorse(std::string input) const
{
    std::string output;
    unsigned int i = 0;
    while (input[i])
    {
        char CH = (input[i] >96 && input[i] < 123 ? input[i]-32 :input[i]);
        unsigned int j = 0;
        while (CH != _vec[j].GetChar() && j < _vec.size())
        {
            ++j;
        }
        if (j < _vec.size())
        {
            output+=_vec[j].GetCode();
            output+=" ";
        }
        else if(CH ==' ')
        {
            output += "   ";
        }
        ++i;
    }
    return output;
}


std::string Morse::FromMorse(std::string input)
{
    std::string output;
    for(unsigned int i = 0;i < input.size();++i)
    {


        if(input[i] == ' ' && input[i+1] == ' '&& input.size() >=i+3)
        {
            output += " ";
            i+=1;

        }
        else
        {
                std::string code = "";
            while(input[i] != ' ' && i < input.size())
            {
                if(input[i] =='*') {input[i] ='.';}
                code+=input[i++];
            }
            for (unsigned int k = 0; k < _vec.size() ; ++k )
            {
                if (code == _vec[k].GetCode())
                {
                    output+= _vec[k].GetChar();
                    break;
                }
            }

        }
    }
    return output;
}


void Morse::newchar(char ch,std::string str)
{
    _vec.push_back(Code(ch,str));
}

/**///////////////////////////////////

void Morse::write(){

for (int i = 0;i < _vec.size() ; ++i)
{
    std::cout << _vec[i].GetChar() << " " << _vec[i].GetCode() << "\n";
}}
