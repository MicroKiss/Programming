
#include <iostream>
#include <fstream>
#include <string>
#include "Morse.h"
#include "Code.h"
using namespace std;


int main()
{
    Morse morse;
    string line;
    ifstream myfile ("morse.dat");
    while  (getline(myfile,line)){
        morse.newchar(line[0],line.substr(2,7));
    }
    myfile.close();
/////////////////
//morse.write();
    //************************////
    string input;
    while(true)
    {
        cout << "1. text to morse" << endl;
        cout << "2. morse to text" << endl;
        cin >> input;
        if (input == "1")
        {
            getline(cin,input);
            cout << "What's the message? : ";

            getline(cin,input);
            cout << "in morse it's: ";
            cout << morse.ToMorse(input);
            cout << endl;
        }
        else if (input == "2")
        {
            getline(cin,input);
            cout << "type your morse code with */. and - characters: ";
            getline(cin,input);
            cout << morse.FromMorse(input);
            cout << endl;
        }//input 2

    }// while true
    return 0;
}
