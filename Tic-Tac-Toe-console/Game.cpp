#include "Game.h"
#include <iostream>
#include <iomanip>
Game::Game()
{
    for (int i = 0; i < 9 ; ++i )
        position[i] = ' ';
    finished = false;
    turnX = true;
}

Game::Start()
{

    while (!finished)
    {
read:
        std::cout << "gimme position (x  y) 1-3: ";
        int x,y;
        std::cin >> y >> x;
        if (position[3*(x-1)+y-1] != ' ')
            goto read;

        position[3*(x-1)+y-1] = (turnX ? 'X' : 'O');
        turnX = !turnX;
        Display();
        checkWinner();
    }
}

void Game::Display()const
{
    for (int i = 0; i < 3 ; ++i )
    {
        std::cout << std::setw(2) << position[3*i] << "|"<< std::setw(2) << position[3*i+1] << "|"<< std::setw(2) << position[3*i+2] << "|";
        std::cout << "\n";
    }
}

void Game::checkWinner()
{
    /** rows*/
    for (int i = 0; i < 3; ++i)
    {
        if(position[3*i+0] == position[3*i+1] && position[3*i+1] == position[3*i+2] && position[3*i] != ' ')
        {
            std::cout << "The winner is " << position[3*i];
            finished = true;
            break;
        }
    }
    /** columns*/
    for (int i = 0; i < 3; ++i)
    {
        if(position[0+i] == position[3+i] && position[3+i] == position[6+i] && position[i] != ' ')
        {
            std::cout << "The winner is " << position[i];
            finished = true;
            break;
        }
    }
    /** \ */

    if (position[0] == position[4] && position[4] == position[8] && position[4] != ' ')
    {
        std::cout << "The winner is " << position[4];
        finished = true;
    }
    // / /
    if (position[3] == position[4] && position[4] == position[6] && position[4] != ' ')
    {
        std::cout << "The winner is " << position[4];
        finished = true;
    }

}
