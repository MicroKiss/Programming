#ifndef GAME_H
#define GAME_H

class Game
{
    public:
        Game();
        Start();
        void Display() const ;
        void checkWinner();

    private:
        char position[9];
        bool finished;
        bool turnX;

};

#endif // GAME_H
