#include <iostream>
#include <stack>

/*
Is the given srtring simmetrycal to the char '#' ?
for example: abc#cbabba#abb#cc#cc is yes
but 		   abc#cbaa#aa           is not    
*/


bool mirror(const std::string &str)
{
    std::stack<char> _S;
    for (int i = 0;i < str.size() ;++i )
    {
      if (str[i] == '#')
      {
        while ( i <str.size() && !_S.empty())
        {
          if(_S.top() != str[++i])
            return false;
          _S.pop();
        }
      }
      else
      {
          _S.push(str[i]);
      }

    }
    return _S.empty();
}


int main()
{
    std::string str;
    std::string testgood[3] = {"abc#cba","abc#cbabba#abb#cc#cc","##"};
    std::string testbad[4] = {"abc","abc#cb","abc#cbaa","abc#cbaa#aa"};

    std::cout << "good: ";
    for(auto i : testgood){
        std::cout << mirror(i)<<" ";
    }
std::cout << "\nrbad : ";
    for(auto i : testbad){
        std::cout << mirror(i)<< " ";
    }
std::cout << std::endl;

    while(1){
    std::cin >> str;
    std::cout <<mirror(str)<< "\n";
}
    return 0;
}
